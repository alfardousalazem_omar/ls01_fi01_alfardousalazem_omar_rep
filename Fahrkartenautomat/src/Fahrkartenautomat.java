﻿import java.util.Scanner;

class Fahrkartenautomat
{
	 public  static double fahrkartenbestellungErfassen() {
		 Scanner tastatur = new Scanner(System.in);
		 double zuZahlenderBetrag = 0.0;
		 double einzahlungsBetrag = 0.0;
		 int Anzahlticket;
		 boolean temp ;
		 boolean temp2 = true;
		 System.out.println("Fahrkartenbestellvorgang:"+"\n");
		 System.out.println("========================="+"\n"+"\n");
		 System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		 System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		 System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
		 System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		 do {
		 System.out.println("Ihre Wahl: ");
		 int wahl =tastatur.nextInt();
		 System.out.println("Anzahl der Tickets: ");
		 Anzahlticket = tastatur.nextInt();
		 if (Anzahlticket >= 10 ) {
			 Anzahlticket = 1 ;
			 System.out.println("Anzahl der Tickets wurde auf 1 gesetzt ");
		 }
		 switch (wahl) {
		 case 1 :
			 zuZahlenderBetrag = Anzahlticket * 2.90;
			temp = false ;
			 break ;
		 case 2 :
			 zuZahlenderBetrag = Anzahlticket * 8.60;
			 temp = false ;
			 break ;
		 case 3 :
			 zuZahlenderBetrag = Anzahlticket * 23.50;
			 temp = false ;
			 break ;
		 default : 
			 System.out.println("falsche Wahl ");
			temp = true ;
					 
		 }
		 
		 }while(temp);
		 
	     return zuZahlenderBetrag;
	       }   
	       
	  public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	       double eingezahlterGesamtbetrag;
	       double eingeworfeneMünze;

			 Scanner tastatur = new Scanner(System.in);

	  eingezahlterGesamtbetrag = 0.0;
      while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
      {
   	   System.out.printf("Noch zu zahlen: %.2f Euro \n",  (zuZahlenderBetrag - eingezahlterGesamtbetrag));
   	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
   	   eingeworfeneMünze = tastatur.nextDouble();
          eingezahlterGesamtbetrag += eingeworfeneMünze;
       
      }
      return eingezahlterGesamtbetrag;
	  }
      public static void fahrkartenAusgeben() {
      
      System.out.println("\nFahrschein wird ausgegeben");
      for (int i = 0; i < 8; i++)
      {
         System.out.print("=");
         try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      }
      System.out.println("\n\n");
      }
	  
       public static void rueckgeldAusgeben (double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	   double rückgabebetrag;
    	   
    	   rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
           if(rückgabebetrag > 0.0)
           {
        	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
        	   System.out.println("wird in folgenden Münzen ausgezahlt:");

               while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
               {
            	  System.out.println("2 EURO");
    	          rückgabebetrag -= 2.0;
               }
               while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
               {
            	  System.out.println("1 EURO");
    	          rückgabebetrag -= 1.0;
               }
               while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
               {
            	  System.out.println("50 CENT");
    	          rückgabebetrag -= 0.5;
               }
               while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
               {
            	  System.out.println("20 CENT");
     	          rückgabebetrag -= 0.2;
               }
               while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
               {
            	  System.out.println("10 CENT");
    	          rückgabebetrag -= 0.1;
               }
               while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
               {
            	  System.out.println("5 CENT");
     	          rückgabebetrag -= 0.05;
               }
           }

           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                              "vor Fahrtantritt entwerten zu lassen!\n"+
                              "Wir wünschen Ihnen eine gute Fahrt.");
       }
        
    public static void main(String[] args)
    {
       
    	Scanner tastatur = new Scanner(System.in);
       boolean temp =true;
       String eingabe;
      
       do {
    	   
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben() ;
       rueckgeldAusgeben ( eingezahlterGesamtbetrag,zuZahlenderBetrag); 
       System.out.println("wollen sie noch ein karte kaufen    "+ "j/n:");
       eingabe = tastatur.next();
       if (eingabe == "j") {
    	temp = false;   
       }
    }  while (temp);
    
}
}
